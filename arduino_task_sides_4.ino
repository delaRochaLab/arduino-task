#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>
#include "CmdMessenger.h"
#include <EEPROM.h>

#define SHIELD_RESET  -1      // VS1053 reset pin (unused!)
#define SHIELD_CS     7      // VS1053 chip select pin (output)
#define SHIELD_DCS    6      // VS1053 Data/command select pin (output)

// These are common pins between breakout and shield
#define CARDCS 4     // Card chip select pin
// DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define DREQ 3       // VS1053 Data request, ideally an Interrupt pin

const int BAUD_RATE = 9600;
const int calibrationPin = 4;
const int calibrationLEDPin = 8;
const int taskLEDPin = 9;

int memoryAddress = 0;
int calPinValue = 0;
int side;
boolean calibration = false;
unsigned long duration;
char box[] = "BOX7"; // Update this variable for each one of the boxes; this name will be the one stored in the calibration log

/* Commands: */
enum {
    play_sound1,
    play_sound2,
    device_ready,
    error,
    send_volume,
    cmd_box,
    save_data
}; 

 /* Initialize messenger: */
CmdMessenger c = CmdMessenger(Serial,',',';','/');

/* Initialize music shield: */
Adafruit_VS1053_FilePlayer musicPlayer = 
  Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);

/* callback */
void on_play_Sound2(void){
    musicPlayer.playFullFile("track002.mp3");
}

/* callback */
void on_play_Sound1(void){
    musicPlayer.playFullFile("track001.mp3");
}

/* callback */
void on_unknown_command(void){
    c.sendCmd(error,"Command without callback.");
}

/* callback */
void on_save_EEPROM(void){
   int calibratedVolume = c.readBinArg<int>();    
   
   if (side == 0){
      // We calibrated L and R, save both: 
      EEPROM.put(memoryAddress, calibratedVolume);
      memoryAddress += sizeof(int);
      EEPROM.put(memoryAddress, calibratedVolume);
   } else if (side == 1){
      // We only calibrated L side, save it at address = 0x00:
      EEPROM.put(memoryAddress, calibratedVolume);
   } else {
      // We only calibrated R side, save it at address = 0x01:
      memoryAddress += sizeof(int);
      EEPROM.put(memoryAddress, calibratedVolume);
   }

   memoryAddress = 0;
}

/* callback */
void on_send_volume(void){
   int volume = c.readBinArg<int>();
   side = c.readBinArg<int>();    
   if (side == 0){
      musicPlayer.setVolume(volume, volume);
   } else if (side == 1){
      musicPlayer.setVolume(volume, -1);
   } else {
      musicPlayer.setVolume(-1, volume);
   }
}


/* callback */
void send_box(void){
   c.sendCmd(cmd_box, box);
}

/* Attach callbacks for CmdMessenger commands */
void attach_callbacks(void) { 
    c.attach(play_sound1, on_play_Sound1);
    c.attach(play_sound2, on_play_Sound2);
    c.attach(send_volume, on_send_volume);
    c.attach(save_data, on_save_EEPROM);
    c.attach(on_unknown_command);
    c.attach(device_ready, send_box);
}

void setup() {
  /* Decide if we want to calibrate or not. If set for calibration, 
  Pin 5 will be connected to Vcc. */
  pinMode(8, OUTPUT);   
  pinMode(9, OUTPUT);
  calPinValue  = analogRead(calibrationPin);
  if (calPinValue > 512){/*Change this if calibration mode doesn't trigger */
    calibration = true;
    digitalWrite(calibrationLEDPin, HIGH);
  } else {
    digitalWrite(taskLEDPin, HIGH);  
  }
  
  Serial.begin(BAUD_RATE);
  if (! musicPlayer.begin()) { 
     while (1);
  }
  if (!SD.begin(CARDCS)) {
    while (1);  
  }
     // Timer interrupts are not suggested, better to use DREQ interrupt!
  //musicPlayer.useInterrupt(VS1053_FILEPLAYER_TIMER0_INT); // timer int

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
}

void loop() {
  Serial.println(box);
  if(calibration == true ){
    /* -------------- Calibration code goes here ----------------------- */
    attach_callbacks(); 
    while(1){
        c.feedinSerialData();
     }
  } else {
     /* -------------- Task code goes here ----------------------- */
     /* First recover the calibrated volume values */
     int volume_R;
     int volume_L;
     EEPROM.get(memoryAddress, volume_L);
     memoryAddress += sizeof(int);
     EEPROM.get(memoryAddress, volume_R);
     musicPlayer.setVolume(volume_L, volume_R);
     memoryAddress = 0;
     Serial.println(volume_L);
     Serial.println(volume_R);
     /* And enter the task: */
     while(1){
        while ((PIND & B00100000) == B00000000); // wait for HIGH
        unsigned long start = micros();                 
        while ((PIND & B00100000) == B00100000);
        unsigned long stop = micros();
        duration = stop-start;
        Serial.println(duration);
        switch(duration) {
            case 300 ... 500:         
              Serial.println(F("Playing track 001"));
              musicPlayer.setVolume(volume_L, -1);
              musicPlayer.startPlayingFile("track001.mp3");
               break;
            case 700 ... 1000:
              Serial.println(F("Playing track 002"));
              musicPlayer.setVolume(-1, volume_R);
              musicPlayer.startPlayingFile("track001.mp3");
              break;
            case 2000 ... 3000:
              Serial.println(F("Playing no calibration mode"));
              musicPlayer.setVolume(1, 1);
              musicPlayer.startPlayingFile("track001.mp3");
              break;
            case 1 ... 200:
              Serial.println(F("Stop playing"));
              musicPlayer.stopPlaying();
              break;              
            case 0:
              digitalWrite(LED_BUILTIN, HIGH); delay(1000);
              digitalWrite(LED_BUILTIN, LOW);  delay(1000);
              break; 
        }
        delay(1);
    }
  }
}
